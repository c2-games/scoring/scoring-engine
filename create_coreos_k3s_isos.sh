#!/bin/bash
# shellcheck disable=SC2046
SRCDIR="$(realpath "$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )")";
BUILDDIR="${SRCDIR}/build"
TMPDIR="${SRCDIR}/tmp"
ENVFILE=${ENVFILE:-"${SRCDIR}/.env"}

# Source variables from ENVFILE
echo "Sourcing variables from ENVFILE: ${ENVFILE}"
# shellcheck disable=SC1090
source "${ENVFILE}" || exit 1

mkdir -p "${BUILDDIR}"
mkdir -p "${TMPDIR}"

cd "${TMPDIR}" || exit 1

# Download latest coreos iso
podman run --privileged --pull=always --rm -v .:/data -w /data \
    quay.io/coreos/coreos-installer:release download \
    --format iso \
    --image-url "${__COREOS_URL}"

FULLISONAME="$(find . -name 'fedora-coreos-*-live.x86_64.iso' -printf '%f\n' | tail -n 1)"
MINIISONAME="${FULLISONAME/.iso/.minimal.iso}"
BUILDISONAME="${__K3S_VERSION}-${FULLISONAME/.iso/.minimal.iso}"

# Make minimal iso
rm -f "${MINIISONAME}"  # make sure it doesn't exist
podman run --security-opt label=disable --pull=always --rm -v .:/data -w /data \
    quay.io/coreos/coreos-installer:release iso extract minimal-iso \
    --rootfs-url "${__COREOS_ROOTFS}" \
    "${FULLISONAME}" \
    "${MINIISONAME}"

ISONAME="${MINIISONAME}"

###
# Generate Control Plane Image ISOs
###
for i in {1..3}; do

    # Copy coreos base image to a newly named iso image
    ## This path might change depending what the latest version is
    cp "${ISONAME}" "k3s-controlplane0${i}-${ISONAME}"

    # Copy base coreos-autoinstall.fcc file
    GENFCC="k3s-controlplane0${i}-coreos-autoinstall.fcc"

    # Substitute any environment variables beginning with '__' into the GENFCC file
    envsubst "$(printf "\${%s} " $(env | grep "^__" | cut -d'=' -f1))" < "${SRCDIR}/coreos-autoinstall-base.fcc.TEMPLATE" > "${GENFCC}"


    # Substitute any environment variables beginning with '__' into each control plane node's autoinstall FCC file
    envsubst "$(printf "\${%s} " $(env | grep "^__" | cut -d'=' -f1))" < "${SRCDIR}/k3s-controlplane0${i}-k3s-autoinstall.fcc.TEMPLATE" > "k3s-controlplane0${i}-k3s-autoinstall.fcc"

    # Embed the k3s-autoinstall ignition file into into the coreos-autoinstall.fcc
    # EXAMPLE: podman run -i --rm quay.io/coreos/fcct:release --pretty --strict < k3s-controlplane01-k3s-autoinstall.fcc | sed 's/^/          /' >> k3s-controlplane01-coreos-autoinstall.fcc
    podman run -i --rm quay.io/coreos/fcct:release --pretty --strict < "k3s-controlplane0${i}-k3s-autoinstall.fcc" | sed 's/^/          /' >> "${GENFCC}"

    # Generate the coreos autoinstall ignition file
    # EXAMPLE: podman run -i --rm quay.io/coreos/fcct:release --pretty --strict < k3s-controlplane01-coreos-autoinstall.fcc > k3s-controlplane01-coreos-autoinstall.ign
    podman run -i --rm quay.io/coreos/fcct:release --pretty --strict < "${GENFCC}" > "k3s-controlplane0${i}-coreos-autoinstall.ign"

    # Slipstream the coreos autoinstall ignition file into the newly named iso image
    # EXAMPLE: podman run --privileged --pull=always --rm -v .:/data -w /data quay.io/coreos/coreos-installer:release iso ignition embed -i k3s-controlplane01-coreos-autoinstall.ign ./k3s-controlplane01-fedora-coreos-35.20220131.3.0-live.x86_64.iso
    podman run --privileged --pull=always --rm -v .:/data -w /data quay.io/coreos/coreos-installer:release iso ignition embed -i "k3s-controlplane0${i}-coreos-autoinstall.ign" "./k3s-controlplane0${i}-${ISONAME}"

    mv "./k3s-controlplane0${i}-${ISONAME}" "${BUILDDIR}/k3s-controlplane0${i}-${BUILDISONAME}"

done


###
# Generate Work Image ISO
###
cp "${ISONAME}" "k3s-workers-${ISONAME}"

# Substitute any environment variables beginning with '__' into the worker autoinstall FCC file
envsubst "$(printf "\${%s} " $(env | grep "^__" | cut -d'=' -f1))" < "${SRCDIR}/coreos-autoinstall-base.fcc.TEMPLATE" > k3s-workers-coreos-autoinstall.fcc

# Substitute any environment variables beginning with '__' into the worker node autoinstall FCC file
envsubst "$(printf "\${%s} " $(env | grep "^__" | cut -d'=' -f1))" < "${SRCDIR}/k3s-workers-k3s-autoinstall.fcc.TEMPLATE" > k3s-workers-k3s-autoinstall.fcc

# Embed the k3s-autoinstall ignition file into the coreos-autoinstall.fcc
podman run -i --rm quay.io/coreos/fcct:release --pretty --strict < k3s-workers-k3s-autoinstall.fcc | sed 's/^/          /' >> k3s-workers-coreos-autoinstall.fcc

# Generate the coreos autoinstall ignition file
podman run -i --rm quay.io/coreos/fcct:release --pretty --strict < k3s-workers-coreos-autoinstall.fcc > k3s-workers-coreos-autoinstall.ign

# Slipstream the coreos autoinstall ignition file into
podman run --privileged --pull=always --rm -v .:/data -w /data quay.io/coreos/coreos-installer:release iso ignition embed -i k3s-workers-coreos-autoinstall.ign "./k3s-workers-${ISONAME}"

mv "./k3s-workers-${ISONAME}" "${BUILDDIR}/k3s-workers-${BUILDISONAME}"
