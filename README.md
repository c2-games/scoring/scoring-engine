# Scoring Engine

This repository houses the Kubernetes setup for the scoring engine.

## Getting Started

### Configuring the ISOs for your environment

Find and configure the variables you need to configure within the `.env` file

```sh
$ grep "CONFIGURE_" .env
DOMAIN="CONFIGURE_YOUR_K3S_DOMAIN"
#checkov:skip=CKV_SECRET_6:Skip check for KV_SECRET_6 on __K3S_TOKEN as this is a placeholder token
export __K3S_TOKEN="CONFIGURE_YOUR_K3S_TOKEN"
export __CORE_USER_PUBLIC_KEY="CONFIGURE_YOUR_SSH_PUBLIC_KEYS"
export __ROOT_USER_PUBLIC_KEY="CONFIGURE_YOUR_SSH_PUBLIC_KEYS"
export __CONTROLPLANE01_PRIMARY_NIC_IP="CONFIGURE_YOUR_CONTROLPLANE_IPS"
export __CONTROLPLANE01_PRIMARY_NIC_CIDRMASK="CONFIGURE_YOUR_CONTROLPLANE_CIDRMASK"
export __CONTROLPLANE01_PRIMARY_NIC_GW="CONFIGURE_YOUR_CONTROLPLANE_GW"
export __CONTROLPLANE02_PRIMARY_NIC_IP="CONFIGURE_YOUR_CONTROLPLANE_IPS"
export __CONTROLPLANE02_PRIMARY_NIC_CIDRMASK="CONFIGURE_YOUR_CONTROLPLANE_CIDRMASK"
export __CONTROLPLANE02_PRIMARY_NIC_GW="CONFIGURE_YOUR_CONTROLPLANE_GW"
export __CONTROLPLANE03_PRIMARY_NIC_IP="CONFIGURE_YOUR_CONTROLPLANE_IPS"
export __CONTROLPLANE03_PRIMARY_NIC_CIDRMASK="CONFIGURE_YOUR_CONTROLPLANE_CIDRMASK"
export __CONTROLPLANE03_PRIMARY_NIC_GW="CONFIGURE_YOUR_CONTROLPLANE_GW"
```

### Generating the ISOs

Run the script to generate the ISOs

```sh
bash ./create_coreos_k3s_isos.sh
```

### Using the ISOs

Boot the controlplane01 iso in a new VM or physical machine and once
that has come alive start controlplane02 followed by controlplane03.
When all the controlplane nodes are online start booting workers using
the worker iso.
