# Scoring Engine IaC

This IaC is used to provision the infrastructure for the scoring engine via a set of "golden image" templates stored on
 the local Proxmox instance.

More Readme updates to come after finalizing this year's cluster golden images.

<!-- markdownlint-disable MD013 -->
<!-- BEGIN_TF_DOCS -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_proxmox"></a> [proxmox](#requirement\_proxmox) | 2.9.14 |

## Providers

No providers.

## Modules

| Name | Source | Version |
|------|--------|---------|
| <a name="module_qemu-from-clone_controlplane"></a> [qemu-from-clone\_controlplane](#module\_qemu-from-clone\_controlplane) | git::https://gitlab.com/c2-games/infrastructure/terraform/proxmox-clone-qemu.git | v1.0.0 |
| <a name="module_qemu-from-clone_k3s_worker"></a> [qemu-from-clone\_k3s\_worker](#module\_qemu-from-clone\_k3s\_worker) | git::https://gitlab.com/c2-games/infrastructure/terraform/proxmox-clone-qemu.git | v1.0.0 |

## Resources

No resources.

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_controlplane_base_id"></a> [controlplane\_base\_id](#input\_controlplane\_base\_id) | The base VMID to use for the controlplane nodes | `string` | n/a | yes |
| <a name="input_storage_pool"></a> [storage\_pool](#input\_storage\_pool) | The storage pool to use | `string` | n/a | yes |
| <a name="input_target_node"></a> [target\_node](#input\_target\_node) | Proxmox node to provision VMs on | `string` | n/a | yes |
| <a name="input_worker_base_id"></a> [worker\_base\_id](#input\_worker\_base\_id) | The base VMID to use for the worker nodes | `string` | n/a | yes |
| <a name="input_controlplane_cores"></a> [controlplane\_cores](#input\_controlplane\_cores) | value for the cores parameter for the controlplane VM | `number` | `8` | no |
| <a name="input_controlplane_disks"></a> [controlplane\_disks](#input\_controlplane\_disks) | The disks to attach to the controlplane nodes | `list(any)` | <pre>[<br>  "30G"<br>]</pre> | no |
| <a name="input_controlplane_max_memory"></a> [controlplane\_max\_memory](#input\_controlplane\_max\_memory) | value for the memory parameter for the controlplane VM | `number` | `8192` | no |
| <a name="input_controlplane_min_memory"></a> [controlplane\_min\_memory](#input\_controlplane\_min\_memory) | value for the balloon parameter for the controlplane VM | `number` | `2048` | no |
| <a name="input_controlplane_networks"></a> [controlplane\_networks](#input\_controlplane\_networks) | value for the networks parameter for the controlplane VM | <pre>map(object(<br>    {<br>      bridge = string<br>      tag    = optional(number)<br>  }))</pre> | <pre>{<br>  "vmbr667_0": {<br>    "bridge": "vmbr667",<br>    "tag": 667<br>  }<br>}</pre> | no |
| <a name="input_controlplane_sockets"></a> [controlplane\_sockets](#input\_controlplane\_sockets) | value for the sockets parameter for the controlplane VM | `number` | `1` | no |
| <a name="input_controlplane_source"></a> [controlplane\_source](#input\_controlplane\_source) | The source VM to clone for the controlplane nodes | `list(any)` | <pre>[<br>  "k3s-controlplane01-gold",<br>  "k3s-controlplane02-gold",<br>  "k3s-controlplane03-gold"<br>]</pre> | no |
| <a name="input_controlplane_vcpus"></a> [controlplane\_vcpus](#input\_controlplane\_vcpus) | value for the vcpus parameter for the controlplane VM | `number` | `8` | no |
| <a name="input_num_workers"></a> [num\_workers](#input\_num\_workers) | The number of worker nodes to provision | `number` | `3` | no |
| <a name="input_pm_api_token_id"></a> [pm\_api\_token\_id](#input\_pm\_api\_token\_id) | Proxmox API Token ID | `string` | `null` | no |
| <a name="input_pm_api_token_secret"></a> [pm\_api\_token\_secret](#input\_pm\_api\_token\_secret) | Proxmox API Token Secret | `string` | `null` | no |
| <a name="input_pm_api_url"></a> [pm\_api\_url](#input\_pm\_api\_url) | Proxmox API URL | `string` | `null` | no |
| <a name="input_pm_password"></a> [pm\_password](#input\_pm\_password) | Proxmox Password | `string` | `null` | no |
| <a name="input_pm_user"></a> [pm\_user](#input\_pm\_user) | Proxmox User | `string` | `null` | no |
| <a name="input_worker_cores"></a> [worker\_cores](#input\_worker\_cores) | value for the cores parameter for the worker VM | `number` | `8` | no |
| <a name="input_worker_disks"></a> [worker\_disks](#input\_worker\_disks) | The disks to attach to the worker nodes | `list(any)` | <pre>[<br>  "30G",<br>  "1G"<br>]</pre> | no |
| <a name="input_worker_max_memory"></a> [worker\_max\_memory](#input\_worker\_max\_memory) | value for the memory parameter for the worker VM | `number` | `8192` | no |
| <a name="input_worker_min_memory"></a> [worker\_min\_memory](#input\_worker\_min\_memory) | value for the balloon parameter for the worker VM | `number` | `2048` | no |
| <a name="input_worker_networks"></a> [worker\_networks](#input\_worker\_networks) | The networks to attach to the worker nodes | <pre>map(object(<br>    {<br>      bridge = string<br>      tag    = optional(number)<br>  }))</pre> | <pre>{<br>  "vmbr3": {<br>    "bridge": "vmbr3"<br>  },<br>  "vmbr667_0": {<br>    "bridge": "vmbr667",<br>    "tag": 667<br>  },<br>  "vmbr667_1": {<br>    "bridge": "vmbr667",<br>    "tag": 667<br>  }<br>}</pre> | no |
| <a name="input_worker_sockets"></a> [worker\_sockets](#input\_worker\_sockets) | value for the sockets parameter for the worker VM | `number` | `1` | no |
| <a name="input_worker_source"></a> [worker\_source](#input\_worker\_source) | The source VM to clone for the worker nodes | `string` | `"k3s-worker-gold"` | no |
| <a name="input_worker_vcpus"></a> [worker\_vcpus](#input\_worker\_vcpus) | value for the vcpus parameter for the worker VM | `number` | `8` | no |

## Outputs

No outputs.
<!-- END_TF_DOCS -->
<!-- markdownlint-enable MD013 -->
