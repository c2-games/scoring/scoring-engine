variable "pm_api_url" {
  description = "Proxmox API URL"
  type        = string
  default     = null
}
variable "pm_user" {
  description = "Proxmox User"
  type        = string
  default     = null
}
variable "pm_password" {
  description = "Proxmox Password"
  type        = string
  sensitive   = true
  default     = null
}

variable "pm_api_token_id" {
  description = "Proxmox API Token ID"
  type        = string
  default     = null
}
variable "pm_api_token_secret" {
  description = "Proxmox API Token Secret"
  type        = string
  sensitive   = true
  default     = null
}

variable "target_node" {
  description = "Proxmox node to provision VMs on"
  type        = string
}


variable "storage_pool" {
  description = "The storage pool to use"
  type        = string
}

variable "controlplane_base_id" {
  description = "The base VMID to use for the controlplane nodes"
  type        = string
}

variable "controlplane_source" {
  description = "The source VM to clone for the controlplane nodes"
  type        = list(any)
  default = [
    "k3s-controlplane01-gold",
    "k3s-controlplane02-gold",
    "k3s-controlplane03-gold",
  ]
}
variable "controlplane_sockets" {
  description = "value for the sockets parameter for the controlplane VM"
  type        = number
  default     = 1
}

variable "controlplane_cores" {
  description = "value for the cores parameter for the controlplane VM"
  type        = number
  default     = 8
}

variable "controlplane_vcpus" {
  description = "value for the vcpus parameter for the controlplane VM"
  type        = number
  default     = 8
}

variable "controlplane_min_memory" {
  description = "value for the balloon parameter for the controlplane VM"
  type        = number
  default     = 2048
}

variable "controlplane_max_memory" {
  description = "value for the memory parameter for the controlplane VM"
  type        = number
  default     = 8192
}

variable "controlplane_disks" {
  description = "The disks to attach to the controlplane nodes"
  type        = list(any)
  default = [
    "30G"
  ]
}

variable "controlplane_networks" {
  description = "value for the networks parameter for the controlplane VM"
  type = map(object(
    {
      bridge = string
      tag    = optional(number)
  }))
  default = {
    "vmbr667_0" = {
      bridge = "vmbr667"
      tag    = 667
    }
  }
}

variable "worker_base_id" {
  description = "The base VMID to use for the worker nodes"
  type        = string
}

variable "num_workers" {
  description = "The number of worker nodes to provision"
  type        = number
  default     = 3
}

variable "worker_source" {
  description = "The source VM to clone for the worker nodes"
  type        = string
  default     = "k3s-worker-gold"
}

variable "worker_sockets" {
  description = "value for the sockets parameter for the worker VM"
  type        = number
  default     = 1
}

variable "worker_cores" {
  description = "value for the cores parameter for the worker VM"
  type        = number
  default     = 8
}

variable "worker_vcpus" {
  description = "value for the vcpus parameter for the worker VM"
  type        = number
  default     = 8
}

variable "worker_min_memory" {
  description = "value for the balloon parameter for the worker VM"
  type        = number
  default     = 2048
}

variable "worker_max_memory" {
  description = "value for the memory parameter for the worker VM"
  type        = number
  default     = 8192
}

variable "worker_disks" {
  description = "The disks to attach to the worker nodes"
  type        = list(any)
  default = [
    "30G",
    "1G"
  ]
}

variable "worker_networks" {
  description = "The networks to attach to the worker nodes"
  type = map(object(
    {
      bridge = string
      tag    = optional(number)
  }))
  default = {
    "vmbr667_0" = {
      bridge = "vmbr667"
      tag    = 667
    },
    "vmbr3" = {
      bridge = "vmbr3"
    }
    "vmbr667_1" = {
      bridge = "vmbr667"
      tag    = 667
    },
  }
}
