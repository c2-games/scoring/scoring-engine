locals {
  controlplane = [
    for i in range(3) : {
      vmid        = "${var.controlplane_base_id}${i + 1}"
      name        = "k3s-cp0${i + 1}"
      description = "K3s Controlplane Node ${i + 1}"
      source      = var.controlplane_source[i]
    }
  ]
  controlplane_node_hardware = {
    disks = {
      for k, v in var.controlplane_disks : "disk_${k}" => {
        type    = "scsi"
        size    = v
        cache   = "writethrough"
        backup  = true
        ssd     = 1
        discard = "on"
        name    = var.storage_pool
      }
    }
    networks = {
      for k, v in var.controlplane_networks : k => {
        interface = lookup(v, "bridge", null)
        tag       = lookup(v, "tag", null)
        firewall  = false
        link_down = false
        model     = "virtio"
      }
    }
  }


  workers = [
    for i in range(1, var.num_workers + 1) : {
      vmid        = "${var.worker_base_id}${i}"
      name        = "k3s-worker0${i}"
      description = "K3s Worker Node ${i}"
      source      = var.worker_source
    }
  ]
  worker_node_hardware = {
    disks = {
      for k, v in var.worker_disks : "disk_${k}" => {
        type    = "scsi"
        size    = v
        cache   = "writethrough"
        backup  = true
        ssd     = 1
        discard = "on"
        name    = var.storage_pool
      }
    }
    networks = {
      for k, v in var.worker_networks : k => {
        interface = lookup(v, "bridge", null)
        tag       = lookup(v, "tag", null)
        firewall  = false
        link_down = false
        model     = "virtio"
      }
    }
  }
}
