module "qemu-from-clone_controlplane" {
  #checkov:skip=CKV_TF_1:Module is using a tagged version
  source = "git::https://gitlab.com/c2-games/infrastructure/terraform/proxmox-clone-qemu.git?ref=v1.0.0"
  for_each = {
    for index, vm in local.controlplane :
    vm.vmid => vm
  }

  VM_NAME                   = each.value.name
  VM_VMID                   = each.value.vmid
  VM_DESC                   = each.value.description
  VM_AGENT                  = true
  VM_BOOT_ORDER             = "order=scsi0"
  VM_SCSI_HW                = "virtio-scsi-pci"
  VM_CLONE                  = each.value.source # Must be a VM Name, not a VMID
  VM_FULL_CLONE             = false
  VM_QEMU_OS                = "l26"
  VM_MEM                    = var.controlplane_max_memory # full amount of mem available for allocation
  VM_MIN_MEM                = var.controlplane_min_memory # minimal amount of mem
  VM_SOCKETS                = var.controlplane_sockets
  VM_CORES                  = var.controlplane_cores
  VM_VCPUS                  = var.controlplane_vcpus
  VM_CPU                    = "host"
  VM_NODE                   = var.target_node
  VM_START_ON_CREATE        = true
  VM_START_ON_BOOT          = false
  VM_DEFINE_CONNECTION_INFO = false
  VM_NETWORKS               = local.controlplane_node_hardware.networks
  VM_DISKS                  = local.controlplane_node_hardware.disks
}


# Provision workers
module "qemu-from-clone_k3s_worker" {
  #checkov:skip=CKV_TF_1:Module is using a tagged version
  source = "git::https://gitlab.com/c2-games/infrastructure/terraform/proxmox-clone-qemu.git?ref=v1.0.0"
  for_each = {
    for index, vm in local.workers :
    vm.vmid => vm
  }

  VM_NAME                   = each.value.name
  VM_VMID                   = each.value.vmid
  VM_DESC                   = each.value.description
  VM_AGENT                  = true
  VM_BOOT_ORDER             = "order=scsi0"
  VM_SCSI_HW                = "virtio-scsi-pci"
  VM_CLONE                  = each.value.source # Must be a VM Name, not a VMID
  VM_FULL_CLONE             = false
  VM_QEMU_OS                = "l26"
  VM_MEM                    = var.worker_max_memory # full amount of mem available for allocation
  VM_MIN_MEM                = var.worker_min_memory # minimal amount of mem
  VM_SOCKETS                = var.worker_sockets
  VM_CORES                  = var.worker_cores
  VM_VCPUS                  = var.worker_vcpus
  VM_CPU                    = "host"
  VM_NODE                   = var.target_node
  VM_START_ON_CREATE        = true
  VM_START_ON_BOOT          = false
  VM_DEFINE_CONNECTION_INFO = false
  VM_NETWORKS               = local.worker_node_hardware.networks
  VM_DISKS                  = local.worker_node_hardware.disks

  depends_on = [
    module.qemu-from-clone_controlplane
  ]
}
